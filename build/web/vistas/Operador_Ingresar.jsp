<%-- 
    Document   : Operador_Ingresar
    Created on : 25 sept 2021, 19:13:38
    Author     : mafe_
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../estilo.css" rel="stylesheet" type="text/css"/>
        <title>Operador Ingresar</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script>
  $( function() {
    $( "#fechaingreso" ).datepicker({
		format :"dd/mm/yyy",
		starDate : '-3d',
		language: "es",
      numberOfMonths: 1,
      showButtonPanel: true
    });
  } );
  
  </script>
</head>
<body background="../imagenes\fondo.jpg" style="background-repeat: no-repeat;" onload="todos();">
    <div id="menu">
        <br></br>
        <br></br>
        <img src="../imagenes\easy_parking_texto.png" class="imagen" alt="Easy Parking">
        <ul class="fondomenu">
            <li>
                <a href="Operador_Ingresar.jsp">Ingresar
                </a>
            </li>
            <li>
                <a href="Operador_Facturar.jsp">Facturar
                </a>
            </li>
            <li>
                <a href="Operador_Consultar.jsp">Consultar
                </a>
            </li>
        </ul>
    </div>
    <div class="table">
        <form action="" method="post" name="form_ingreso">
        <div>		
            <table >
                <tr>
                    <td align="center" colspan="4">
                        <font color="#FFFFFF"><h1>INGRESAR</h1></font>
                    </td>
                </tr>
                <tr>		
                    <td align="right">
                        <font>Tipo de <br>vehículo</font>
                    </td>
                    <td align="left">
                        <select id="tipo_vehiculo">
                            <option>Automovil</option>
                            <option>Moto</option>
                        </select>
                    </td>
                    <td align="right"><font>Placa</font>
                    </td>
                    <td align="left">
                        <input type="text" id="placa">
                    </td>
                </tr>
<!--                <tr>		
                    <td align="right">
                        <font>Valor por <br>minuto $</font>
                    </td>
                
                    <td align="left">
                <script>
                    function values()
                    {
                        var datos=document.getElementById("tipo_vehiculo");
                        val=datos.options[datos.selectedIndex].text;
                        if(val === "Automovil")
                        {
                            var valor = 110;
                            document.getElementById("vr_minuto").value = valor;
                        }
                        else
                        {
                            var valor = 77;
                            document.getElementById("vr_minuto").value = valor;
                        }
                    }
                    
                </script>
                <input type="text" id="vr_minuto" name="vr_minuto" >
                    </td>
                    <td align="center" colspan="2">
                        <input type="button" value="Calcular" name="Calcular" class="boton" id="calcular" onclick="return values();">
                    </td>
                </tr>-->
                <tr>
                    <td align="right">
                        <font>Fecha</font>
                    </td>
                    <td align="left">
                        <input id="fechaingreso" name="fechaingreso">
                        
                    <script type="text/javascript">
                    function todos()
                    {
                        SetDate();
                        mueveReloj();
                    }
                        function SetDate()
                    {
                    var date = new Date();

                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();

                    if (month < 10) month = "0" + month;
                    if (day < 10) day = "0" + day;

                    var today = year + "-" + month + "-" + day;


                    document.getElementById('fechaingreso').value = today;
                    }
                    function mueveReloj(){
                        momentoActual = new Date();
                        hora = momentoActual.getHours();
                        minuto = momentoActual.getMinutes();
                        horaImprimible = hora + " : " + minuto;
                        document.form_ingreso.hora_ingreso.value = horaImprimible;
                        setTimeout("mueveReloj()",1000);
                    }
                    </script>
                    </td> 
                    <td align="right">
                        <font>Hora de<br>Ingreso</font>
                    </td>
                    <td align="left">
                        <input type ="text" name="hora_ingreso" id="hora_ingreso">
                    </td>
                </tr>
                <tr>		
                    <td align="right">
                        <font>Plaza</font>
                    </td>
                    <td align="left">
                        <input type="text" id="plaza">
                    </td>
                </tr>
                <tr>    		
                    <td colspan="2">
                        <center>
                            <input name="Registar" type="submit" value="Registar" metod="post" class="boton">
                        </center>
                    </td>
                    <td  colspan="2">
                        <center>
                            <input type="button" onclick="location.href='../index.jsp'" value="Salir" class="boton"/>
                        </center>
                    </td>
                </tr>
            </table>
        </div>
        </form>
    </div>
</body>
</html>