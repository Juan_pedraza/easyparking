<%-- 
    Document   : Administrador_Facturar
    Created on : 25 sept 2021, 20:48:27
    Author     : mafe_
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador Facturar</title>
        <link href="../estilo.css" rel="stylesheet" type="text/css"/>
        <script>
  $( function() {
    $( "#fechaingreso" ).datepicker({
		format :"dd/mm/yyy",
		starDate : '-3d',
		language: "es",
      numberOfMonths: 1,
      showButtonPanel: true
    });
  } );
  </script>
    </head>
 <body background="..\Imagenes\fondo.jpg" style="background-repeat: no-repeat;" >
    <div id="menu">
        <br></br>
        <br></br>
        <img src="..\Imagenes\easy_parking_texto.png" class="imagen" alt="Easy Parking">
        <ul class="fondomenu">
            <li>
                <a href="Administrador_Ingresar.jsp">Ingresar
                </a>
            </li>
            <li>
                <a href="Administrador_Facturar.jsp">Facturar
                </a>
            </li>
            <li>
                <a href="Administrador_Consultar.jsp">Consultar
                </a>
            </li>
            <li>
                <a href="Administrador_Usuarios.jsp">Usuarios
                </a>
            </li>
            <li>
                <a href="Administrador_Informes.jsp">Informes
                </a>
            </li>
	</ul>
    </div>
    <div class="table">
        <form action="" method="post" name="administrador_facturar">
            <div>		
                <table >

				<tr>
					<td align="center" colspan="4">
						<font color="#FFFFFF"><h1>MODIFICAR FACTURACION</h1></font>
					</td>
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Placa</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="placa_factura">
					</td>
					<td  align="center" colspan="2">
							<input name="Consultar_placa" type="submit" value="Consultar" metod="post" class="boton">
					</td>
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Tipo de <br>vehículo</font>
						</label>
					</td>
					<td align="left">
						<font color="#FFFFFF">
						<input type="text" id="Tipo_Vehiculo">
						</font>
					</td>
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Valor por<br>minuto</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="vr_minuto">
					</td>
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Valor por <br>hora</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="vr_hora">
					</td>
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Hora de <br>Ingreso</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="h_ingreso">
					</td>
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
                                                    <font color="#FFFFFF">Fecha de <br> Ingreso</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="fecha_ingreso">
					</td>
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Plaza</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="plaza">
					</td>
				</tr>
				<tr>		
					<td align="right">
						
						<label for="text">
                                                    <font color="#FFFFFF">Hora de<br> Salida</font>
						</label>
					</td>
					<td align="left">
						<input type="time" id="h_salida">
					</td>
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Fecha</font>
						</label>
					</td>
					<td align="left">
						<input type = "date" name="fecha_ingreso" value="" id="fechaEgreso" class="form-control" placeholder="Click aqui" data-date-format="dd/mm/yyy"/>
					</td>
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Total a <br>Pagar</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="total_pagar">
					</td>
				</tr>
				<tr>
					<td align="center" colspan="4">
						<center>
							<input name="Imprimir" type="submit" value="Eliminar" metod="post" class="boton">  
					
							<input name="Imprimir" type="submit" value="Modificar" metod="post" class="boton">  
					
							<input name="Imprimir" type="submit" value="Imprimir" metod="post" class="boton">  
						</center>
					</td>
				</tr>
			</table>
		</div>
		</form>
	</div>
</body>
</html>
