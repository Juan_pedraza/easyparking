<%-- 
    Document   : Administrador_Informes
    Created on : 25 sept 2021, 20:52:01
    Author     : mafe_
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador Informes</title>
        <link href="../estilo.css" rel="stylesheet" type="text/css"/>
        <script>
  $( function() {
    $( "#fechaingreso" ).datepicker({
		format :"dd/mm/yyy",
		starDate : '-3d',
		language: "es",
      numberOfMonths: 1,
      showButtonPanel: true
    });
  } );
  </script>
    </head>
    <body background="..\Imagenes\fondo.jpg" style="background-repeat: no-repeat;" >
    <div id="menu">
        <br></br>
        <br></br>
        <img src="..\Imagenes\easy_parking_texto.png" class="imagen" alt="Easy Parking">
        <ul class="fondomenu">
            <li>
                <a href="Administrador_Ingresar.jsp">Ingresar
                </a>
            </li>
            <li>
                <a href="Administrador_Facturar.jsp">Facturar
                </a>
            </li>
            <li>
                <a href="Administrador_Consultar.jsp">Consultar
                </a>
            </li>
            <li>
                <a href="Administrador_Usuarios.jsp">Usuarios
                </a>
            </li>
            <li>
                <a href="Administrador_Informes.jsp">Informes
                </a>
            </li>
	</ul>
    </div>
    <div class="table">
        <form action="" method="post" name="administrador_informes">
            <div>		
                <table class="tabla">

				<tr>
					<td align="center" colspan="3">
						<font color="#FFFFFF"><h1>INFORMES</h1></font>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Fecha</font>
						</label>
					</td>
					<td align="left">
						<input type = "date" 
							   name="fecha_informes" 
							   value="" id="fechaInformes" 
							   class="form-control" 
							   placeholder="Click aqui" 
							   data-date-format="dd/mm/yyyy"/>
					</td>
					<td align="lef">
						<input name="Consultar_fecha" type="submit" value="Consultar" metod="post" class="boton">
					</td>
				</tr>
				<tr>		
					<td align="center" colspan="3">
						<center>
							<hr size="2px" color="white" />
						</center>
					</td>
				</tr>
				<tr>
					<td align="center">
						<input name="Imprimir_facturacion" type="submit" value="Imprimir facturación" metod="post" class="boton">
					</td>
					<td align="center">
						<input name="Imprimir_plazas" type="submit" value="Imprimir plazas" metod="post" class="boton">
					</td>
					<td align="center">
						<input name="Imprimir_usuarios" type="submit" value="Imprimir usuarios" metod="post" class="boton">
					</td>
					
				</tr>
			</table>
		</div>
	</form>
</body>
</html>