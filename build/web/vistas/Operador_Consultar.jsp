
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="ModeloDAO.vehiculoDAO"%>
<%@page import="Modelo.Tipo_vehiculo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Operador Consultar</title>
        <link href="../estilo.css" rel="stylesheet" type="text/css"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

        <script>
            $(function () {
                $("#fechaingreso").datepicker({
                    format: "dd/mm/yyy",
                    starDate: '-3d',
                    language: "es",
                    numberOfMonths: 1,
                    showButtonPanel: true
                });
            });
        </script>
    </head>
    <body background="..\imagenes\fondo.jpg" style="background-repeat: no-repeat;" >
        <div id="menu">
            <br></br>
            <br></br>
            <img src="..imagenes\easy_parking_texto.png" class="imagen" alt="Easy Parking">
            <ul class="fondomenu">
                <li>
                    <a href="Operador_Ingresar.jsp">Ingresar
                    </a>
                </li>
                <li>
                    <a href="Operador_Facturar.jsp">Facturar
                    </a>
                </li>
                <li>
                    <a href="Operador_Consultar.jsp">Consultar
                    </a>
                </li>
            </ul>
        </div>
        <div class="table">
            
            
            
            <table border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>PLACA</th>
                    <th>TIPO_VEHÍCULO</th>
                    <th>HORA_INGRESO</th>
                    <th>FECHA_INGRESO</th>
                </tr>
            </thead>
            <%
                vehiculoDAO dao = new vehiculoDAO();
                
                List<Tipo_vehiculo>list=dao.listar();
                
                Iterator<Tipo_vehiculo>iter=list.iterator();
                
                Tipo_vehiculo car = null;
                
                while(iter.hasNext()){
                    car=iter.next();
                
            
            %>
            <tbody>
                <tr>
                    <td> <%= car.getId() %> </td>
                    <td> <%= car.getPlaca() %> </td>
                    <td> <%= car.getTipo_vehiculo() %> </td>
                    <td> <%= car.getHora_ingreso() %> </td>
                    <td> <%= car.getFecha_ingreso() %> </td>
                </tr>
                <% } %>
            </tbody>
        </table>
            
            
            
            
<!--            <form action="" method="post">
                <div>		
                    <table>
                        <tr>
                            <td align="center" colspan="4">
                                <font color="#FFFFFF"><h1>CONSULTAR</h1></font>
                            </td>
                        </tr>
                        <tr>		
                            <td align="right">
                                <label for="text">
                                    <font color="#FFFFFF">Placa</font>
                                </label>
                            </td>
                            <td align="left">
                                <input type="text" id="placa_factura">
                            </td>
                            <td align="left" colspan="3">
                                <input name="Consultar_placa" onclick="Controller?accion = operador_ingresar" type="submit" value="Consultar" metod="post" class="boton">
                            </td>
                        </tr>
                        <tr>		
                            <td align="right">
                                <label for="text">
                                    <font color="#FFFFFF">Tipo de <br>vehículo</font>
                                </label>
                            </td>
                            <td align="left">
                                <font color="#FFFFFF">
                                <input type="text" id="Tipo_Vehiculo">
                                </font>
                            </td>
                            <td align="right">
                                <label for="text">
                                    <font color="#FFFFFF">Hora de <br>Ingreso</font>
                                </label>
                            </td>
                            <td align="left">
                                <input type="text" id="h_ingreso">
                            </td>
                        </tr>
                        <tr>		
                            <td align="right">
                                <label for="text">
                                    <font color="#FFFFFF">Fecha de <br>Ingreso</font>
                                </label>
                            </td>
                            <td align="left">
                                <input type="text" id="fecha_ingreso" >
                            </td>
                            					<td align="right">
                                                                            <label for="text">
                                                                                    <font color="#FFFFFF">Plaza</font>
                                                                            </label>
                                                                    </td>
                            <td align="left">
                                <input type="text" id="plaza">
                            </td>
                        </tr>
                        <tr>		
                            <td align="center" colspan="4">
                        <center>
                            <hr size="2px" color="white" />
                        </center>
                        </td>
                        </tr>
                        <tr>		
                            <td align="right">
                                <font color="#FFFFFF">Plazas</font>
                            </td>
                            <td align="left">
                                <input name="Consultar_placa"  type="submit" value="Consultar" metod="post" class="boton">
                            </td>
                            <td align="right">
                                <label for="text">
                                    <font color="#FFFFFF">Plazas <br>dispobibles</font>
                                </label>
                            </td>
                            <td align="left">
                                <input type="text" id="plazas_dispobibles">
                            </td>
                        </tr>
                    </table>
                </div>
            </form>-->
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

    </body>
</html>