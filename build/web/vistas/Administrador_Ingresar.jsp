<%-- 
    Document   : Administrador_Ingresar
    Created on : 25 sept 2021, 20:16:13
    Author     : mafe_
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador Ingresar</title>
        <script>
  $( function() {
    $( "#fechaingreso" ).datepicker({
		format :"dd/mm/yyy",
		starDate : '-3d',
		language: "es",
      numberOfMonths: 1,
      showButtonPanel: true
    });
  } );

  </script>
<link href="../estilo.css" rel="stylesheet" type="text/css"/>
    </head>
<body background="../Imagenes/fondo.jpg" style="background-repeat: no-repeat;" >
    <div id="menu">
        <br></br>
        <br></br>
        <img src="../Imagenes/easy_parking_texto.png" class="imagen" alt="Easy Parking">
        <ul class="fondomenu">
            <li>
                <a href="Administrador_Ingresar.jsp">Ingresar
                </a>
            </li>
            <li>
                <a href="Administrador_Facturar.jsp">Facturar
                </a>
            </li>
            <li>
                <a href="Administrador_Consultar.jsp">Consultar
                </a>
            </li>
            <li>
                <a href="Administrador_Usuarios.jsp">Usuarios
                </a>
            </li>
            <li>
                <a href="Administrador_Informes.jsp">Informes
                </a>
            </li>
	</ul>
    </div>
    <div class="table">
        <form action="../Controller" >
            <div>		
                <table>

				<tr>
					<td align="center" colspan="4">
						<font color="#FFFFFF"><h1>MODIFICAR INGRESO</h1></font>
					</td>
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Placa</font>
						</label>
					</td>
					<td align="left">
                                            <input name="consultar_placa" type="text" id="consultar_placa">
					</td>
                                        <td colspan="2">
						<input name="Consultar_placa" type="submit" value="Consultar" metod="post" class="boton">
					</td>
				</tr>
				<tr>
					<td align="right">
						<label for="text">
                                                    <font color="#FFFFFF">Tipo de <br> vehículo</font>
						</label>
					</td>
					<td align="left">
						<font color="#FFFFFF">
                                                <select name="Tipo_vehiculo">
 							<option>Automóvil</option>
  							<option>Moto</option>
						</select>
						</font>
					</td>	
<!--					<td align="right">
						<label for="text">
                                                    <font color="#FFFFFF">Valor por <br>minuto</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="vr_minuto">
					</td>
				</tr>
				<tr>
					
					<td align="right">
						<label for="text">
                                                    <font color="#FFFFFF">Valor por <br>hora</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="vr_hora">
					</td>-->
						
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Hora de<br>Ingreso</font>
						</label>
					</td>
					<td align="left">
                                            <!--<input type="time" id="h_ingreso" 
                                                       value="<?php 
                                                       $time= time ();
                                                       echo date('time');?>"/>-->
                                       
                                            <input name="hora_ingreso" type="time" id="hora_ingreso">
					
                                        </td>
				</tr>
				<tr>
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Fecha</font>
						</label>
					</td>
					<td align="left">
					    	    <input type = "date"  
							   name="fecha_ingreso" 
							   value="<?php
                                         echo date('d/m/Y');
                                        ?>" 
                                        id="fecha_ingreso" 
							   
							 /> 
                                        
					</td>
						
<!--					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Plaza</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="plaza">
					</td>-->
				</tr>
                                <tr>
                                    <td><br><br></td>
                                </tr>
				<tr>		
                                    <td colspan="4">
						<center>
                                                    
                                                    <input name= "accion" type="submit" value="Registro" class="boton">
                                                    
                                                    <input name="Modificar" type="submit" value="Modificar" metod="post" class="boton">  
						
							<input name="Modificar" type="submit" value="Eliminar" metod="post" class="boton">  
						
							<input type="button" onclick="location.href='../index.jsp';" value="Salir" class="boton"/>
                                                        
						</center>
					</td>
					
				</tr>
			</table>
		</div>
           
		</form>
	</div>
</body>

</html>