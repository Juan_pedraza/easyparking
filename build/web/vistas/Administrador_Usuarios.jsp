<%-- 
    Document   : Administrador_Usuarios
    Created on : 25 sept 2021, 20:54:28
    Author     : mafe_
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador Usuarios</title>
        <link href="../estilo.css" rel="stylesheet" type="text/css"/>
        <script>
  $( function() {
    $( "#fechaingreso" ).datepicker({
		format :"dd/mm/yyy",
		starDate : '-3d',
		language: "es",
      numberOfMonths: 1,
      showButtonPanel: true
    });
  } );
  </script>
    </head>
<body background="..\Imagenes\fondo.jpg" style="background-repeat: no-repeat;" >
    <div id="menu">
        <br></br>
        <br></br>
        <img src="..\Imagenes\easy_parking_texto.png" class="imagen" alt="Easy Parking">
        <ul class="fondomenu">
            <li>
                <a href="Administrador_Ingresar.jsp">Ingresar
                </a>
            </li>
            <li>
                <a href="Administrador_Facturar.jsp">Facturar
                </a>
            </li>
            <li>
                <a href="Administrador_Consultar.jsp">Consultar
                </a>
            </li>
            <li>
                <a href="Administrador_Usuarios.jsp">Usuarios
                </a>
            </li>
            <li>
                <a href="Administrador_Informes.jsp">Informes
                </a>
            </li>
	</ul>
    </div>
    <div class="table">
        <form action="" method="post" name="administrador_ingrear">
            <div>		
                <table >

				<tr>
					<td align="center" colspan="4">
						<font color="#FFFFFF"><h1>USUARIOS</h1></font>
					</td>
				</tr>
				<tr>
				<td align="right">
						<label for="text">
                                                    <font color="#FFFFFF">Tipo de <br> documeto</font>
						</label>
					</td>
					<td align="left">
						<font color="#FFFFFF">
						<select>
 							<option>Cédula de Ciudadanía</option>
  							<option>Cédula de Extranjería</option>
							<option>Pasaporte</option>
						</select>
						</font>
					</td>
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Número de <br>documento</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="n_documento">
					</td>
				</tr>
				<tr>
				<td align="right">
						<label for="text">
							<font color="#FFFFFF">Nombres</font>
						</label>
					</td>
					<td align="left">
						<font color="#FFFFFF">
						<input type="text" id="nombres">
						</font>
					</td>
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Apellidos</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="apellidos">
					</td>
					
				</tr>
				<tr>
				<td align="right">
						<label for="text">
							<font color="#FFFFFF">Rol</font>
						</label>
					</td>
					<td align="left">
						<font color="#FFFFFF">
						<select>
 							<option>Administrador</option>
  							<option>Operador</option>
						</select>
						</font>
					</td>
					
					</tr>
				<tr>
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Usuario</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="usuario">
					</td>
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Contraseña</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="contrasena">
					</td>
				</tr>
                                <tr><td><br><br></td></tr>
				<tr>
					<td align="center">
						<input name="Consultar_placa" type="submit" value="Consultar" metod="post" class="boton">
					</td>
					<td align="center">
						<input name="Consultar_placa" type="submit" value="Registrar" metod="post" class="boton">
					</td>
					<td align="center">
						<input name="Consultar_placa" type="submit" value="Modificar" metod="post" class="boton">
					</td>
					<td align="center">
						<input name="Consultar_placa" type="submit" value="Eliminar" metod="post" class="boton">
					</td>
				</tr>
			</table>
		</div>
		</form>
	</div>
</body>
</html>