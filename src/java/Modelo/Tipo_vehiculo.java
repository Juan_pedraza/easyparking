/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author juanpedraza
 */
public class Tipo_vehiculo {
    int id;
    String placa, tipo_vehiculo;
    String hora_ingreso,fecha_ingreso;

    public Tipo_vehiculo() {
    }

    public Tipo_vehiculo(int id, String placa, String tipo_vehiculo, String hora_ingreso, String fecha_ingreso) {
        this.id = id;
        this.placa = placa;
        this.tipo_vehiculo = tipo_vehiculo;
        this.hora_ingreso = hora_ingreso;
        this.fecha_ingreso = fecha_ingreso;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getTipo_vehiculo() {
        return tipo_vehiculo;
    }

    public void setTipo_vehiculo(String tipo_vehiculo) {
        this.tipo_vehiculo = tipo_vehiculo;
    }

    public String getHora_ingreso() {
        return hora_ingreso;
    }

    public void setHora_ingreso(String hora_ingreso) {
        this.hora_ingreso = hora_ingreso;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }
    
    
    
    
    
    
    
    
}
