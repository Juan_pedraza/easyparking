package ModeloDAO;

import Config.Conexion;
import Interface.crud;
import Modelo.Tipo_vehiculo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author juanpedraza
 */
public class vehiculoDAO implements crud {

    Conexion con_bd = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    Tipo_vehiculo car = new Tipo_vehiculo();

    @Override
    public List listar() {

        ArrayList<Tipo_vehiculo> list = new ArrayList<>();

        String sql = "Select * from Tipo_vehiculo";

        try {
            con = con_bd.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                Tipo_vehiculo car = new Tipo_vehiculo();
                car.setId(rs.getInt("Id"));
                car.setPlaca(rs.getString("Placa"));
                car.setTipo_vehiculo(rs.getString("tipo_vehiculo"));
                car.setHora_ingreso(rs.getString("hora_ingreso"));
                car.setFecha_ingreso(rs.getString("fecha_ingreso"));

                list.add(car);
            }

        } catch (Exception e) {
            System.out.println("Error en la consulta sql" + e);
        }

        return list;

    }

    @Override
    public Tipo_vehiculo list(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(Tipo_vehiculo car) {

        String sql = "INSERT INTO Tipo_vehiculo(placa, tipo_vehiculo,hora_ingreso_fecha_ingreso) VALUES ('"+car.getPlaca()+"', '"+car.getTipo_vehiculo()+"', '"+car.getHora_ingreso()+"', '"+car.getFecha_ingreso()+"')";

        try {

            con = con_bd.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println("Error actualizando el registro de la BD" + e);
        }

        return false;

    }

    @Override
    public boolean edit(Tipo_vehiculo car) {
        String sql = "UPDATE Tipo_vehiculo SET placa='"
                + car.getPlaca() + "', tipo_vehiculo='"
                + car.getTipo_vehiculo() + "',hora_ingreso='"
                + car.getHora_ingreso() + "', fecha_ingreso='"
                + car.getFecha_ingreso() + "' WHERE ID="
                + car.getId();

        try {

            con = con_bd.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println("Error actualizando el registro de la BD" + e);
        }

        return false;
    }

    @Override
    public boolean delete(int id) {
        String sql = "DELETE FROM Tipo_vehiculo WHERE ID=" + id;

        try {

            con = con_bd.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println("Error eliminando el registro de la BD" + e);
        }

        return false;

    }

}
