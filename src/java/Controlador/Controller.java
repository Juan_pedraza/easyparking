/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Tipo_vehiculo;
import ModeloDAO.vehiculoDAO;
import javax.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author juanpedraza
 */
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    String adminConsultar = "vistas/Administrador_consultar.jsp";
    //String test = "vistas/test.jsp";
    String add = "vistas/Administrador_Ingresar.jsp";
    
    
    
    Tipo_vehiculo car = new Tipo_vehiculo();
    
    vehiculoDAO dao = new vehiculoDAO();
    
    int id;
            
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String acceso= "";
        String action= request.getParameter("accion");
        if(action.equalsIgnoreCase("adminConsultar")){
            acceso=adminConsultar;
        }else{
            if(action.equalsIgnoreCase("add")){
                acceso=add;
                
            }else if(action.equalsIgnoreCase("Registro")){
                 String placa = request.getParameter("consultar_placa");
                 String tipo_vehiculo = request.getParameter("Tipo_vehiculo");
                 String hora_ingreso = request.getParameter("hora_ingreso");
                 String fecha_ingreso = request.getParameter("fecha_ingreso");
                 car.setPlaca(placa);
                 car.setTipo_vehiculo(tipo_vehiculo);
                 car.setHora_ingreso(hora_ingreso);
                 car.setFecha_ingreso(fecha_ingreso);
                 dao.add(car);
            }else{
                if(action.equalsIgnoreCase("Eliminar")){
                    id = Integer.parseInt(request.getParameter("id"));
                    car.setId(id);
                    dao.delete(id);
                    
                    
                }
            }
        }
        
        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
        
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
