
package Interface;

import Modelo.Tipo_vehiculo;
import java.util.List;

/**
 *
 * @author juanpedraza
 */
public interface crud {
    public List listar();
    public Tipo_vehiculo list(int id);
    public boolean add(Tipo_vehiculo carro);
    public boolean edit(Tipo_vehiculo carro);
    public boolean delete(int id);
}
