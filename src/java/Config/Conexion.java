
package Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;


/**
 *
 * @author juanpedraza
 */
public class Conexion {
    
    Connection con;

    
    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Easy1","root","Abc123**");
            
            
        } catch (Exception e) {
            System.out.println("Error al conectar con la base de datos");
        }
    }
    
    public Connection getConnection()
    {
        return con;
    }

    
    
    
    
}
