<%-- 
    Document   : Operador_Facturar
    Created on : 25 sept 2021, 20:37:43
    Author     : mafe_
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Operador Facturar</title>
        <link href="../estilo.css" rel="stylesheet" type="text/css"/>
        <script>
  $( function() {
    $( "#fechaingreso" ).datepicker({
		format :"dd/mm/yyy",
		starDate : '-3d',
		language: "es",
      numberOfMonths: 1,
      showButtonPanel: true
    });
  } );
  </script>
</head>
<body background="..\Imagenes\fondo.jpg" style="background-repeat: no-repeat;" onload="mueveReloj()">
    <div id="menu">
        <br></br>
        <br></br>
        <img src="..\Imagenes\easy_parking_texto.png" class="imagen" alt="Easy Parking">
        <ul class="fondomenu">
            <li>
                <a href="Operador_Ingresar.jsp">Ingresar
                </a>
            </li>
            <li>
                <a href="Operador_Facturar.jsp">Facturar
                </a>
            </li>
            <li>
                <a href="Operador_Consultar.jsp">Consultar
                </a>
            </li>
          	</ul>
    </div>
    <div class="table">
		<form action="" method="post" name="form_salida">
		<div>		
			<table >
				<tr>
					<td align="center" colspan="4">
						<font color="#FFFFFF"><h1>FACTURAR</h1></font>
					</td>
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Placa</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="placa_factura">
					</td>
					<td>
						<center>
							<input name="Consultar_placa" type="submit" value="Consultar" metod="post" class="boton">
						</center>
					</td>
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
                                                    <font color="#FFFFFF">Tipo de <br>vehículo</font>
						</label>
					</td>
					<td align="left">
						<font color="#FFFFFF">
                                                    <input type="text" id="Tipo_Vehiculo">
						</font>
					</td>
					<td align="right">
						<label for="text">
                                                    <font color="#FFFFFF">Valor por<br>minuto</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="vr_minuto">
					</td>
				</tr>
				<tr>		
					
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Fecha de<br>Ingreso</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="fecha_ingreso">
					</td>
                                        <td align="right">
						<label for="text">
							<font color="#FFFFFF">Hora de<br>Ingreso</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="h_ingreso">
					</td>
				</tr>
				<tr>		
                                    <td align="right">
						
						<label for="text">
							<font color="#FFFFFF">Hora de <br>Salida</font>
						</label>
					</td>
					  <script type="text/javascript">
                                            function mueveReloj(){
                                                momentoActual = new Date();
                                                hora = momentoActual.getHours();
                                                minuto = momentoActual.getMinutes();
                                                horaImprimible = hora + " : " + minuto;
                                                document.form_salida.h_salida.value = horaImprimible;
                                                setTimeout("mueveReloj()",1000);
                                            }
                    </script>
                                        <td align="left">
						<input type="text" id="h_salida">
					</td>
                                    <td align="right">
						<label for="text">
							<font color="#FFFFFF">Minutos <br>Facturados</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="minutos_facturados">
					</td>
                                        
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Subtotal :</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="subtotal">
					</td><td align="right">
						<label for="text">
							<font color="#FFFFFF">IVA :</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="iva">
					</td>
					
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Total a <br>Pagar :</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="total_pagar">
					</td>
				</tr>
				<tr>		
					<td colspan="4">
						<center>
							<input name="Imprimir" type="submit" value="Imprimir" metod="post" class="boton">
						</center>
					</td>
				</tr>
			</table>
		</div>
		</form>
	</div>
</body>
</html>