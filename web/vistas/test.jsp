<%-- 
    Document   : test
    Created on : 2/10/2021, 4:46:07 p. m.
    Author     : juanpedraza
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="Modelo.Tipo_vehiculo"%>
<%@page import="ModeloDAO.vehiculoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <table border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>PLACA</th>
                    <th>TIPO_VEHÍCULO</th>
                    <th>HORA_INGRESO</th>
                    <th>FECHA_INGRESO</th>
                </tr>
            </thead>
            <%
                vehiculoDAO dao = new vehiculoDAO();
                
                List<Tipo_vehiculo>list=dao.listar();
                
                Iterator<Tipo_vehiculo>iter=list.iterator();
                
                Tipo_vehiculo car = null;
                
                while(iter.hasNext()){
                    car=iter.next();
                
            
            %>
            <tbody>
                <tr>
                    <td> <%= car.getId() %> </td>
                    <td> <%= car.getPlaca() %> </td>
                    <td> <%= car.getTipo_vehiculo() %> </td>
                    <td> <%= car.getHora_ingreso() %> </td>
                    <td> <%= car.getFecha_ingreso() %> </td>
                </tr>
                <% } %>
            </tbody>
        </table>

    </body>
</html>
