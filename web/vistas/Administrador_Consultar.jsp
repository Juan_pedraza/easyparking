<%-- 
    Document   : Administrador_Consultar
    Created on : 25 sept 2021, 20:46:21
    Author     : mafe_
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="Modelo.Tipo_vehiculo"%>
<%@page import="ModeloDAO.vehiculoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador Consultar</title>
        <link href="../estilo.css" rel="stylesheet" type="text/css"/>
        <script type ="text/javascript">
  $( function() {
    $( "#fechaingreso" ).datepicker({
		format :"dd/mm/yyy",
		starDate : '-3d',
		language: "es",
      numberOfMonths: 1,
      showButtonPanel: true
    });
  } );
  </script>

</head>
<body background="..\Imagenes\fondo.jpg" style="background-repeat: no-repeat;" >
    <div id="menu">
        <br></br>
        <br></br>
        <img src="..\Imagenes\easy_parking_texto.png" class="imagen" alt="Easy Parking">
        <ul class="fondomenu">
            <li>
                <a href="Administrador_Ingresar.jsp">Ingresar
                </a>
            </li>
            <li>
                <a href="Administrador_Facturar.jsp">Facturar
                </a>
            </li>
            <li>
                <a href="Administrador_Consultar.jsp">Consultar
                </a>
            </li>
            <li>
                <a href="Administrador_Usuarios.jsp">Usuarios
                </a>
            </li>
            <li>
                <a href="Administrador_Informes.jsp">Informes
                </a>
            </li>
	</ul>
    </div>
    <div class="table">
        <form action="Controller" method="post" name="administrador_consultar">
            <div>		
                <table >

				<tr>
					<td align="center" colspan="4">
						<font color="#FFFFFF"><h1>CONSULTAR PLAZAS</h1>
						</font>
					</td>
				</tr>
				<tr>		
					<td align="right">
						<label for="text">
							<font color="#FFFFFF">Plaza</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="plaza">
					</td>
				</tr>
				<tr>		
					<td align="center" colspan="4">
						<center>
							<hr size="2px" color="white" />
						</center>
					</td>
				</tr>
				<tr>		
					<td align="right">
						<font color="#FFFFFF">Plazas</font>
					</td>
					<td align="left">
						<input name="Consultar_placa" type="submit" value="Consultar" metod="post" class="boton">
					</td>
					<td align="left">
						<label for="text">
							<font color="#FFFFFF">Plazas dispobibles</font>
						</label>
					</td>
					<td align="left">
						<input type="text" id="plazas_dispobibles">
					</td>
					
				</tr>
				<tr>
					<td align="center" colspan="4">
						<input name="Imprimir_plazas" type="submit" value="Imprimir plazas" metod="post" class="boton">
					</td>
					
				</tr>
			</table>
		</div>
		</form>
	</div>
    <a href="Controller?accion=add">Registrar Vehículo</a>
    <table border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>PLACA</th>
                    <th>TIPO_VEHÍCULO</th>
                    <th>HORA_INGRESO</th>
                    <th>FECHA_INGRESO</th>
                </tr>
            </thead>
            <%
                vehiculoDAO dao = new vehiculoDAO();
                
                List<Tipo_vehiculo>list=dao.listar();
                
                Iterator<Tipo_vehiculo>iter=list.iterator();
                
                Tipo_vehiculo car = null;
                
                while(iter.hasNext()){
                    car=iter.next();
                
            
            %>
            <tbody>
                <tr>
                    <td> <%= car.getId() %> </td>
                    <td> <%= car.getPlaca() %> </td>
                    <td> <%= car.getTipo_vehiculo() %> </td>
                    <td> <%= car.getHora_ingreso() %> </td>
                    <td> <%= car.getFecha_ingreso() %> </td>
                    <td><a href="Controller?accion=editar&id= <%= car.getId()%>">Editar</a></td>
                    <td><a href="Controller?accion=eliminar&id= <%= car.getId()%>">Eliminar</a></td>
                </tr>
                <% } %>
            </tbody>
        </table>
</body>
</html>
