
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="estilo1.css"/>
        <title>Easy Parking</title>
    </head>
    <body background="imagenes/fondo.jpg" style="background-repeat: no-repeat;" >
        <div class="table">
            <form name="login">
                <div>
                    <br></br>
                    <br></br>
                    <img src="imagenes/easy_parking_texto.png" class="imagen" alt="Easy Parking">
                    <br></br>
                    <br></br>
                    <table>
                        <tr>		
                            <th align="right" scope="row">
                                <font color="#FFFFFF">Usuario:
                                </font>
                            </th>
                            <td align="left" height="33">
                                <span class="cnt">
                                    <input type="text" id="usuario" class="Input" name="usuario" size="20">
                                </span>
                            </td>
                            <td rowspan="2" align="left" width="55" height="53">
                                <img src="imagenes/vector_Candado_r.png" width="53" height="53" alt="">
                            </td>
                        </tr>
                        <tr>
                            <th align="right" scope="row">
                                <font color="#FFFFFF">Contraseña:
                                </font>
                            </th>
                            <td align="left">
                                <span class="cnt">
                                    <input type="password" name="password" id="password" class="Input" value="" size ="20">
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <span class="cnt">
                                    <input value="Ingresar" target="_parent" onclick="Login()" type="button" class="boton" style="width:80px; height:40px"/>
                                </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
        
<!--        <div>
            <a href="Controller?accion=test" >Prueba</a>
        </div>-->
        
        <script language="JavaScript">
            function Login()
            {
                var done = 0;
                var usuario = document.login.usuario.value;
                var password = document.login.password.value;
                var pagina = "vistas/Administrador_Ingresar.jsp";
                var pagina1 = "vistas/Registrar.jsp";
                if (usuario === "Admin" && password === "Admin")
                {
                    document.location.href = pagina;
                } else if (usuario === "Operador" && password === "Operador")
                {
                    document.location.href = pagina1;
                } else if (usuario === "" && password === "")
                {
                    window.alert("Los campos usuario y contraseña no pueden estar vacios");
                } else
                {
                    window.alert("Error en usuario y/o contraseña");
                }
            }
        </script> 	  
        <script language="Javascript">
            <!-- Begin 
                document.oncontextmenu = function () {
                return false
            }
            // End --> 
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                //Disable cut copy paste
                $('body').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });

                //Disable mouse right click
                $("body").on("contextmenu", function (e) {
                    return false;
                });
            });
            </script>
        </body>
    </html>
